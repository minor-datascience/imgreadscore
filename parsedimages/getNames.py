from os import listdir
from os.path import isfile, join
from PIL import Image, ImageEnhance
from diff_match_patch import diff_match_patch
import cv2
import pytesseract
import numpy as np
import re
import pymongo

DATABASE_NAME = "goodreads"
COLLECTION_MINE_NAME = "full_parse_results"  # table/collection name where parsed data is written

# TODO fix save location of pre-processed images
IMAGE_LOCATION = "./"

rip = ['9780007236336', '9780007247097', '9780007331697', '9780008167677', '9780060899226', '9780061172113', '9780061767937', '9780071452939', '9780099469391', '9780099493082', '9780099539551', '9780141183336', '9780141189222', '9780141376677', '9780143785217', '9780199570713', '9780241203576', '9780241301111', '9780425168134', '9780425284629', '9780435449308', '9780439155878', '9780439376129', '9780470523988', '9780486264431', '9780486409702', '9780553815863', '9780634003202', '9780648287346', '9780674504806', '9780715324530', '9780715338360', '9780715338377', '9780752876818', '9780752881676', '9780753553206', '9780762408283', '9780762459377', '9780785134817', '9780785190363', '9780860681939', '9780915299140', '9780958402613', '9780975767757', '9780975767764', '9780984074426', '9780996485272', '9781101911761', '9781118134610', '9781118749104', '9781118960875', '9781138806306', '9781401307783', '9781406308129', '9781406328738', '9781407136639', '9781409516972', '9781416507697', '9781433816789', '9781444706383', '9781446302446', '9781452111889', '9781452137575', '9781471147999', '9781473657663', '9781476730622', '9781501101496', '9781506362946', '9781510105003', '9781552662816', '9781578634019', '9781586638535', '9781593271978', '9781593276003', '9781594744471', '9781594773785', '9781595620255', '9781597180733', '9781599186115', '9781608870080', '9781628600018', '9781742207544', '9781780224725', '9781782062387', '9781782214441', '9781782217107', '9781782435693', '9781784298470', '9781785301827', '9781785920745', '9781786571588', '9781786572875', '9781787470439', '9781844084807', '9781844285457', '9781844482511', '9781844488049', '9781844489350', '9781847175915', '9781849057790', '9781856048101', '9781857998658', '9781863514743', '9781904955498', '9781940655062', '9781942021698', '9788498381498', '9788867324491', 'getNames.py']

print(f"AMOUNT OF DEAD ISBNS: {rip.__sizeof__()}")
# database connection
db_client = pymongo.MongoClient(
    "mongodb://jort:finalround@jort.dev")
db = db_client[DATABASE_NAME]
db_collection_mine = db[COLLECTION_MINE_NAME]
book_dict_list = []
isbn_cursor = db_collection_mine.find({})
for book_dict in isbn_cursor:
    book_dict_list.append(book_dict)

dmp = diff_match_patch()
onlyFiles = [f for f in listdir("./") if isfile(join("./", f))]
print(onlyFiles)


def calculateScore(text, match):
    matchLoc = dmp.match_main(text, match, round(len(text) / 2))  # Find location of closest match
    fromText = text[matchLoc: matchLoc + len(match)]  # Extract closest matched string
    score = dmp.diff_levenshtein(dmp.diff_main(fromText, match)) / len(match)  # Calculate difference using Levenshtein
    return score

faultyIsbns = []

print(f"LENGTH OF LIST IS {onlyFiles.__sizeof__()}")
for x in onlyFiles:
    try:
        isbn = x.replace(".jpg","")
        title = ""
        author = ""
        for book_dict in book_dict_list:
            if book_dict["isbn"] == isbn:
                title = book_dict["title"].upper()
                author = book_dict["writer"].upper()
                break
        print(f"Title: {title} \nAuthor: {author} \nISBN: {isbn}")
        originalImage = cv2.imread(IMAGE_LOCATION + x)
        grayImage = cv2.cvtColor(originalImage, cv2.COLOR_BGR2GRAY)  # grayscale image
        # Create kernel
        kernel = np.array([[0, -1, 0],
                           [-1, 5, -1],
                           [0, -1, 0]])
        # Sharpen image
        image_sharp = cv2.filter2D(grayImage, -1, kernel)
        cv2.imwrite(f"{IMAGE_LOCATION}processedimages/{x}", image_sharp)
        coverText = pytesseract.image_to_string(Image.open(f"{IMAGE_LOCATION}processedimages/{x}"))  # use OCR to read text from cover
        parsedText = re.sub(r'\W+', ' ', coverText).upper()  # Make OCR output 1 line and UPPERcase
        print("PARSED DATA\n", parsedText)
        titleScore = calculateScore(parsedText, title)
        authorScore = calculateScore(parsedText, author)
        print("Title score = ", titleScore, "\n", "Author score = ", authorScore, "\n")
        mydict = {"titleScore": titleScore, "authorScore": authorScore}
        db_collection_mine.update_one({"isbn":isbn}, {"$set": mydict})

    except:
        faultyIsbns.append(isbn)
        print(f"Exception with: {isbn}")


print(f"---------THESE ISBNS DID NOT HAVE VALID IMAGES----------\n{faultyIsbns}\n ---------------------------------------------------------")

